import React, { Component } from 'react';

import {Grid, Row, Col, Button, FormGroup, FormControl} from 'react-bootstrap';
import ListContainer from './modules/container';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider'

import Modal from 'react-modal';
import Measure from 'react-measure';

Modal.setAppElement('#root')

class App extends Component {
  constructor(props, context) {
    super(props, context);
    this.child = React.createRef();

    this.handleChange = this.handleChange.bind(this);

    this.state = {
      value: '',
      toggle: "false",
      modalIsOpen: false,
      dimensions: {
        width: -1,
        height: -1,
      },
      modalArr:[],
      searchClicked:false
    };

    this.openModal = this.openModal.bind(this);
    this.closeModal = this.closeModal.bind(this);
  }

  openModal() {
    if(this.state.searchClicked===false)
      this.setState({modalIsOpen: true});
  }

  closeModal() {
    this.setState({modalIsOpen: false});
  }

  modalSearchList = (value, search) => {
    var arr = [];
    value.forEach((item)=>{
      if(this.state.toggle==="false")
        arr.push(item.title);
      else
        arr.push(item.name);
    })
    this.setState({modalArr:arr});

    if(arr.length>0 && search)
      this.openModal()
    else
      this.closeModal()
  }

  onClick = (value) => {
    this.setState({toggle:value, searchClicked:false});
  };

  handleChange(e) {
    if(e.target.value.length>2)
      this.openModal();

    this.setState({ value: e.target.value });
  }

  DidMount = (node) => {
    if (node) {
      node.addEventListener('scroll', () => {
        this.closeModal();
      });
    }
  };

  render() {
    const {top, height, left, width} = this.state.dimensions;
    return (
        <div id="app" ref={this.DidMount} style={{display:"flex", height:"100%", overflowY:"auto"}}>
          <Grid style={container}>
            <Row>
              <Col lg={6}  md={6} xs={3} >
                <Button block bsStyle="primary" onClick={() => this.onClick("false")}>Movies</Button>
              </Col>
              <Col lg={6}  md={6} xs={3}>
                <Button block bsStyle="primary" onClick={() => this.onClick("true")}>TV shows</Button>
              </Col>
            </Row>
            <Row style={{display:"flex", marginTop:20}}>
              <form style={{flex:1}}>
                <Measure
                  bounds
                  onResize={
                    contentRect => {
                      this.setState({dimensions : contentRect.bounds})
                    }
                  }
                >
                  {
                    ({measureRef}) => (
                      <div
                        ref={measureRef}
                      >
                        <FormGroup
                          controlId="formBasicText"
                          validationState="success"
                        >
                          <FormControl
                            type="text"
                            placeholder="Search"
                            onChange={this.handleChange}
                            value={this.state.value}
                          />
                          <FormControl.Feedback />
                        </FormGroup>
                      </div>
                    ) 
                  }
                </Measure>
              </form>
            </Row>
            <Row style={listContainer} id="row">
              <MuiThemeProvider>
                {
                  this.state.toggle==="true" ?
                    <ListContainer ref={this.child} type={"tv"} searchQuery={this.state.value} onSearch={this.modalSearchList}/>
                  :
                    <ListContainer ref={this.child} type={"movie"} searchQuery={this.state.value} onSearch={this.modalSearchList}/>
                }
              </MuiThemeProvider>
            </Row>
          </Grid>
          <Modal
            shouldFocusAfterRender={false}
            style={{
              overlay: {
                backgroundColor: "rgba(0,0,0,0)"
              },
              content: {
                color: 'lightsteelblue',
                top:top+height,
                width:width,
                left:left-8,
                height:100,
                display:"fixed"
              }
            }}
            isOpen={this.state.modalIsOpen}
            onRequestClose={this.closeModal}
          >
            {
              this.state.modalArr.map((item, i)=>
                {
                  return <h6 key={i} onClick={()=>{
                    this.closeModal();
                    this.setState({value:item, searchClicked:true})}
                  }>{item}</h6>
                }
              )
            }
          </Modal>
        </div>
    );
  }
}

const container = {
  paddingTop:20, 
  display:"flex",
  flexDirection:"column" 
};
const listContainer = {
  flex:1, 
  justifyContent:"center",
  padding:20
};

export default App;