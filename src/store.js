import { createStore, combineReducers, applyMiddleware } from 'redux';
import thunkMiddlware from 'redux-thunk';
import { createLogger } from 'redux-logger'
import moviesReducer from './modules/reducers/movies.reducers';

const rootReducer = combineReducers({
  movies: moviesReducer
});

const loggerMiddleware = createLogger();

const store = createStore(
  rootReducer,
  undefined,
    applyMiddleware(
      thunkMiddlware,
      loggerMiddleware
    )
);

export default store;