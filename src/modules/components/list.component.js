import React from 'react';
import {Row, Col} from 'react-bootstrap';
import CardComponent from './card.component';
import LoaderComponent from '../common/loader.component';

const styles = {
  movieColumn: {
    marginBottom: 20
  }
}
const ListComponent = ({movies, isLoading, type}) => {
  const movieColumns = movies ? movies.map(movie => (
    <Col style={styles.movieColumn} key={movie.popularity+movie.id.toString()} xs={12} sm={4} md={3} lg={3}>
      <CardComponent movie={movie} type={type}/>
    </Col>
  )) : null;
  
  return (
    <Row>
      {movieColumns}
      <LoaderComponent isLoading={isLoading} />
    </Row>
  );
}

export default ListComponent;