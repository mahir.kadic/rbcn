import React from 'react';
import {Card, CardTitle, CardMedia} from 'material-ui';

const styles = {
  cardTitle: {
    whiteSpace: 'nowrap',
    textOverflow: 'ellipsis',
    overflow: 'hidden'
  },
  cardMedia: {
    maxHeight: 394,
    overflow: 'hidden'
  },
  card: {
    cursor: 'pointer',
    //height: 400,
    overflow: 'hidden'
  },
  bgImage: {
    width: '100%'
  }
};


class CardComponent extends React.Component {
  constructor(props) {
    super(props);
    // Track if the mouse hovering over the movie card
    this.state = {
      isMouseOver: false
    };
  }
  
  render() {
    const {movie, openMovieModal, type} = this.props;
    const subtitle = this.state.isMouseOver ? movie.overview : null;
    const title = type ==="movie" ? movie.title : movie.name;
    return (
      <div style={{display:"inline"}} id="card" onClick={()=>this.setState({modalIsOpen:true})}>
        <Card
          style={styles.card}
          onMouseOver={() => this.setState({isMouseOver: true})}
          onMouseLeave={() => this.setState({isMouseOver: false})}
        >
          <CardMedia
            style={styles.cardMedia}
            overlay={
              <CardTitle
                title={title} 
                subtitle={subtitle} 
              />
            }
          >
            <img style={styles.bgImage} src={movie.poster_path} />
          </CardMedia>
        </Card>
      </div>
    );
  }
}

export default CardComponent;