import {createAsyncActionCreator} from '../common/redux.helpers';
import * as listService from '../api/list.service';

export const keys = {
  'GET_TOP_TEN': 'GET_TOP_TEN',
  'SEARCH': 'SEARCH',
  'GET_DETAILS': 'GET_DETAILS',
};

export const getTopTen = (page, type) => createAsyncActionCreator(
  keys.GET_TOP_TEN,
  listService.getTopTen, 
  {page, type}
);

export const search = (query, page, type) => createAsyncActionCreator(
  keys.SEARCH,
  listService.search, 
  {query, page, type}
);

export const getDetails = (movieId, type) => createAsyncActionCreator(
  keys.GET_DETAILS,
  listService.getDetails, 
  {movieId, type}
);