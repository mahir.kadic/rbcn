import {combineReducers}                      from 'redux';
import { createReducer, createAsyncReducer }  from '../common/redux.helpers';
import { keys as movieActionKeys }            from '../actions/movies.action';

const moviesSuccessReducer = (state, action) => {
  var {results} = action.response;
  const existingMovies = [];
  const sliceLength = results.length > 10 ? 10 : results.length;
  return {
    ...state,
    isLoading: false,
    response: {
      ...action.response,
      results: [
        ...existingMovies,
        ...results.slice(0,sliceLength)
      ]
    }
  };
}

const moviesReducer = combineReducers({
  topTen: createAsyncReducer(movieActionKeys.GET_TOP_TEN, {
    [`${movieActionKeys.GET_TOP_TEN}_SUCCESS`]: moviesSuccessReducer
  }),
  Search: createAsyncReducer(movieActionKeys.SEARCH, {
    [`${movieActionKeys.SEARCH}_SUCCESS`]: moviesSuccessReducer
  }),
});

export default moviesReducer;