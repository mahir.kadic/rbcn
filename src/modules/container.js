import React, {Component} from 'react';
import {connect} from 'react-redux';
import * as movieActions from './actions/movies.action';
import * as Helpers from './common/helpers';
import ListComponent from './components/list.component';

class ListContainer extends Component {
  constructor(props) {
    super(props);
    this.state = {
      toggle: "movie",
      search:false,
      query:"",
      modalIsOpen:false
    };

  }
  
  componentDidMount() {
    this.props.getTopTen(1, this.state.toggle);
  }

  componentWillReceiveProps(nextProps){
    var {type, searchQuery} = nextProps;

    if(searchQuery.length>=3 && (this.state.query !== searchQuery || type!== this.state.toggle)){
      this.props.search(searchQuery, 1, type);
      this.setState({search:true, query:searchQuery, toggle:type})

    }else if(this.state.toggle!==type || (searchQuery.length < 3 && this.state.search)){
      this.props.getTopTen(1, type);
      this.setState({toggle: type, search: false});
    }
  }


  render() {
    const {topMovies, searchList} = this.props;
    const active = this.state.search? searchList : topMovies ;
    const list = this.state.search? Helpers.getList(searchList.response) : Helpers.getList(topMovies.response);
    if(list!==null)
      this.props.onSearch(list.slice(0,2), this.state.search);

    return (
      <div id="list">
        <ListComponent movies={list} isLoading={active.isLoading} type={this.state.toggle}/>
      </div>
    );
  }
}

export default connect(
  (state) => ({
    topMovies: state.movies.topTen,
    searchList: state.movies.Search
  }),
  { ...movieActions }
)(ListContainer);